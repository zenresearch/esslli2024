# Zen Research - Experimental methodology in computational linguistics
#### OR: Stressing less about our (questionable) experimental choices

<br/>


Welcome the repository of the [ESSLLI 2024](https://2024.esslli.eu/) course **Experimental methodology in computational linguistics**, nicknamed **Zen Research**. The course is of _introductory_ level and is designed for trainees in the _Language and Computation (LaCo)_ area, although parts of it may be interesting for people working with data in general. The course is taught by:

  * [Carlos Ramisch](http://pageperso.lis-lab.fr/carlos.ramisch/) - [`first.last@lis-lab.fr`](mailto:first.last@lis-lab.fr)
  * [Manon Scholivet](https://dblp.org/pid/211/7445.html) - [`first.last@universite-paris-saclay.fr`](mailto:first.last@universite-paris-saclay.fr)

Here you will find all predagogical material and useful links to be used during and after the course.


TODO list for the participants
------------------------------

**Before the course**: Good news! There are no preparatory activities required for this course.

**During the course**: The course features interactive activities on **Wooclap**. You will be able to participate if you have a device (smartphone, tablet, or computer) connected to the internet.

In addition to raising your arm and speak, you will be able to ask questions in written form using this [shared Google document for live questions](https://docs.google.com/document/d/1vTa1-42YppMR-1sZ0yr26abge3MRZT681r6bepFn8Tc/edit?usp=sharing), which will be editable by all participants during the course sessions.

**After the course**: help us enhance our teaching skills by filling in the [feedback form](https://forms.gle/7u4d5gLnhneUcoaS8). Thanks a lot!

_Note:_ The use of Wooclap and Google tools is not mandatory. By using them, you agree to the [Wooclap data policy](https://www.wooclap.com/en/privacy-policy/), [Google forms data policy](https://transparency.google/our-policies/product-terms/google-forms/) and [Google docs data policy](https://transparency.google/our-policies/product-terms/google-docs/). Have a look at the linked websites if you are unsure about how your data will be used by these companies.

Course outline
---------------

The course will take place in the [2nd week of ESSLLI](https://2024.esslli.eu/placeholder-programme/course-schedule.html), on August 5-9, at 9:00-10:30.

  * **Day 1 (Aug 5): Defining our research question**
    * [Slides (PDF)](slides/CM1-research-question-SotA.pdf)
    * [Wooclap link](https://app.wooclap.com/ZENRS1) - or enter `ZENRS1` at [wooclap.com](https://www.wooclap.com)
  * **Day 2 (Aug 6): Reading a scientific paper**
    * [Slides (PDF)](slides/CM2-critical-article-reading.pdf)
    * [Article to read - English version (PDF)](exercises/fake-article-biased.pdf)
    * [Article to read - French version (PDF)](exercises/fake-article-biased-french.pdf)
    * [Wooclap link](https://app.wooclap.com/ZENRS2) - or enter `ZENRS2` at [wooclap.com](https://www.wooclap.com)

  * **Day 3 (Aug 7): Preparing the data**
    * [Slides (PDF)](slides/CM3-data-preparation.pdf)
    * [Inter-annotator agreement tool (ZIP)](exercises/kappa-zenresearch.zip)
    * [Wooclap link](https://app.wooclap.com/ZENRS3) - or enter `ZENRS3` at [wooclap.com](https://www.wooclap.com)
  * **Day 4 (Aug 8): Analysing our results**
    * [Slides (PDF)](slides/CM4-results-analysis.pdf)
    * [Jupyter notebook (ZIP)](exercises/Course4_zen-research_data-analysis.zip)
    * [Wooclap link](https://app.wooclap.com/ZENRS4) - or enter `ZENRS4` at [wooclap.com](https://www.wooclap.com)
  * **Day 5 (Aug 9): Sharing our findings**
    * [Slides PDF (PDF)](slides/CM5-communication.pdf)
    * [Wooclap link](https://app.wooclap.com/ZENRS5) - or enter `ZENRS5` at [wooclap.com](https://www.wooclap.com)

Useful links
------------

  * [Original Zen Research course (in French)](https://pageperso.lis-lab.fr/carlos.ramisch/?page=recherchezen)
  * [Course description and rationale - ESSLLI course proposal](ESSLLI_2024_PUBLIC_proposal-Experimental_methodology_in_CL.pdf)
  * [Shared document for live questions (Google docs)](https://docs.google.com/document/d/1vTa1-42YppMR-1sZ0yr26abge3MRZT681r6bepFn8Tc/edit?usp=sharing)
  * [Course feedback form (Google form)](https://forms.gle/7u4d5gLnhneUcoaS8)


Further reading
---------------  

  * Ron Artstein, Massimo Poesio. 2008. [Survey Article: Inter-Coder Agreement for Computational Linguistics](10.1162/coli.07-034-R2). _Computational Linguistics_, 34(4):555–596.
  * Nesrine Bannour, Sahar Ghannay, Aurélie Névéol, Anne-Laure Ligozat. 2021. [Evaluating the carbon footprint of NLP methods: a survey and analysis of existing tools](https://aclanthology.org/2021.sustainlp-1.2/). _Proc. of SustaiNLP 2021_, p. 11–21, Online.
  * Taylor Berg-Kirkpatrick, David Burkett, Dan Klein. 2012. [An Empirical Investigation of Statistical Significance in NLP](https://aclanthology.org/D12-1091/). _Proc. of EMNLP-CoNLL 2012_, p. 995–1005, Jeju Island, Korea.
  * Abeba Birhane, Vinay Uday Prabhu, Emmanuel Kahembwe. 2021. [Multimodal datasets: misogyny, pornography, and malignant stereotypes](https://arxiv.org/abs/2110.01963). _ArXiv.  
  * Peter Bruce, Andrew Bruce, Peter Gedeck. 2020. [Practical Statistics for Data Scientists, 2nd Ed.](https://www.oreilly.com/library/view/practical-statistics-for/9781492072935/). O'Reilly Media, Inc.
  * Mine Çetinkaya-Rundel, Johanna Hardin. 2022. [Modern Statistical Methods for Psychology](https://bookdown.org/gregcox7/ims_psych/).
  * Rotem Dror, Gili Baumer, Segev Shlomov, Roi Reichart. 2018. [The Hitchhiker’s Guide to Testing Statistical Significance in Natural Language Processing](https://aclanthology.org/P18-1128/). _Proc. of ACL 2018_, p. 1383–1392, Melbourne, Australia.
  * Rotem Dror, Lotem Peled-Cohen, Segev Shlomov, Roi Reichart. 2020. [Statistical Significance Testing for Natural Language Processing](https://doi.org/10.1007/978-3-031-02174-9). Springer.
  * Stephen Few. 2007. [Save the pies for dessert](https://www.perceptualedge.com/articles/visual_business_intelligence/save_the_pies_for_dessert.pdf). _Visual Business Intelligence Newsletter_.
  * Hila Gonen, Yoav Goldberg. 2019. [Lipstick on a Pig: Debiasing Methods Cover up Systematic Gender Biases in Word Embeddings But do not Remove Them](https://aclanthology.org/W19-3621/). _Proc. of WiNLP 2019_, p. 60–63, Florence, Italy.
  * Kyle Gorman, Steven Bedrick. 2019. [We Need to Talk about Standard Splits](https://aclanthology.org/P19-1267/). _Proc. of ACL 2019_, p. 2786–2791, Florence, Italy.
  * J. Richard Landis and Gary G. Koch. 1977. [The Measurement of Observer Agreement for Categorical Data](https://doi.org/10.2307/2529310). _Biometrics_, 33(1):159-174.
  * Marvin Lanhenke. 2022 [Understanding Random Variables and Probability Distributions](https://towardsdatascience.com/understanding-random-variables-and-probability-distributions-1ed1daf2e66). _Towards Data Science_. Medium.
  * Yann Mathet, Antoine Widlöcher. 2016. [Évaluation des annotations : ses principes et ses pièges (Annotation evaluation: principles and pitfalls)](https://aclanthology.org/2016.tal-2.4/). _Traitement Automatique des Langues_, 57(2):73–98. (In French)  
  * Mohit Pandey. 2023. [OpenAI Might Go Bankrupt by the End of 2024](https://analyticsindiamag.com/openai-might-go-bankrupt-by-the-end-of-2024/). _Analytics India Magazine_
  * David Patterson, Joseph Gonzalez, Quoc Le, Chen Liang, Lluis-Miquel Munguia, Daniel Rothchild, David So, Maud Texier, Jeff Dean. 2021. [Carbon Emissions and Large Neural Network Training](https://arxiv.org/abs/2104.10350). _ArXiv_
  * Carlos Ramisch, Abigail Walsh, Thomas Blanchard, Shiva Taslimipoor. 2023. [A Survey of MWE Identification Experiments: The Devil is in the Details](https://aclanthology.org/2023.mwe-1.15/). _Proc. of MWE 2023_, p. 106–120, Dubrovnik, Croatia.
  * Anders Søgaard, Sebastian Ebert, Jasmijn Bastings, Katja Filippova. 2021. [We Need To Talk About Random Splits](https://aclanthology.org/2021.eacl-main.156/). _Proc. of EACL 2021_, p. 1823–1832, Online.
  * Emma Strubell, Ananya Ganesh, Andrew McCallum. 2019. [Energy and Policy Considerations for Deep Learning in NLP](https://aclanthology.org/P19-1355/). _Proc. of ACL 2019_, p. 3645–3650, Florence, Italy.
  * Alexander Yeh. 2000. [More accurate tests for the statistical significance of result differences](https://aclanthology.org/C00-2137/). _Proc. of COLING 2000_.
  * Jieyu Zhao, Tianlu Wang, Mark Yatskar, Vicente Ordonez, Kai-Wei Chang. 2017. [Men Also Like Shopping: Reducing Gender Bias Amplification using Corpus-level Constraints](https://aclanthology.org/D17-1323/). _Proc. of EMNLP 2017_, p. 2979–2989, Copenhagen, Denmark.

  
**Youtube channels**

  * [_DATAtab_ Youtube channel](https://www.youtube.com/@datatab)
  * [_Hygiène Mentale_ Youtube channel (in French)](https://www.youtube.com/c/Hygi%C3%A8neMentale)
  * [_Le Chat Sceptique_ Youtube channel (in French)](https://www.youtube.com/@ChatSceptique)
  * [_Monsieur Phi_ Youtube channel (in French)](https://www.youtube.com/@MonsieurPhi)
  * [_StatQuest_ Youtube channel](https://www.youtube.com/@statquest)


License
-------

All the material in this repository, including all course slides and exercises, is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)


Acknowledgments
---------------

In additional to our bibliographical references, this courses builds on our experience as researchers in computational linguistics, including interactions with many colleagues and friends, who often shared valuable suggestions and/or pedagogical materials. We are grateful, among others, to Elie Antoine, Thomas Blanchard, Marie Candito, Damien Driot, Benoit Favre, François Hamonic, Anna Mosolova, Alexis Nasr, Adeline Paiement, Lorreine Petters, Shiva Taslimipoor, Marius Thorre, Abigail Walsh, and to all participants of previous editions of this course at Aix Marseille University and Université de Toulon.

This README is adapted from the [ESSLLI 2018 course "MWEs in a Nutshell"](https://gitlab.com/parseme/mwesinanutshell/) by Agata Savary, Aline Villavicencio, and Carlos Ramisch.
